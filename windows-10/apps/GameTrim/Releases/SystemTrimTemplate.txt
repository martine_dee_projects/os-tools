LF
say: ==================================================
say: Corsair Trim
say: ==================================================
LF

service: CorsairGamingAudioConfig # Corsair Gaming Audio Configuration Service
service: CorsairLLAService # Corsair iCUE Helper Service
service: CorsairMsiPluginService # Corsair MSI Plugin Service
service: CorsairService

taskkill: iCUE.exe
taskkill: Corsair.Service.exe
taskkill: Corsair.Service.CpuIdRemote64.exe
taskkill: Corsair.Service.DisplayAdapter.exe
taskkill: CorsairMsiPluginService.exe
taskkill: CueLLAccessService.exe

LF
say: ==================================================
say: Asus Trim
say: ==================================================
LF

service: ArmouryCrateService # ARMOURY CRATE Service
service: asComSvc # ASUS Com Service
service: asHmComSvc # Asus HM Com Service
service: AsusCertService
service: AsusFanControlService
service: AsusROGLSLService # AsusROGLSLService Download ROGLSLoader
service: AsusUpdateCheck
service: ROG Live Service # ROG Live Service
service: LightingService

taskkill: Aac3572DramHal_x86.exe
taskkill: Aac3572MbHal_x86.exe
taskkill: AacKingstonDramHal_x86.exe
taskkill: AcPowerNotification.exe
taskkill: ArmouryAIOFanServer.exe
taskkill: ArmourySocketServer.exe
taskkill: ArmourySwAgent.exe
taskkill: asus_framework.exe
taskkill: AsusUpdate.exe
taskkill: extensionCardHal_x86.exe
taskkill: ROGLiveService.exe
taskkill: SonicRadar3.exe # Probably needed if you use the mobo sound (I don't)
taskkill: SonicStudio3.exe # Probably needed if you use the mobo sound (I don't)
LF

LF
say: ==================================================
say: Razer Trim
say: ==================================================
LF

service: Razer Central Service
service: Razer Chroma SDK Server
service: Razer Chroma SDK Service
service: Razer Game Manager Service
service: Razer Synapse Service
service: RzActionSvc # Razer Central Service
service: RzKLService

taskkill: FPSRunner32.exe
taskkill: FPSRunner64.exe
taskkill: GameManagerService.exe
taskkill: PMRunner32.exe
taskkill: PMRunner64.exe
taskkill: RzKLService.exe
taskkill: RazerCortex.exe
taskkill: RazerCentralService.exe
taskkill: Razer Central.exe
taskkill: Razer Synapse Service.exe
taskkill: Razer Synapse Service Process.exe
taskkill: Razer Synapse 3.exe
taskkill: RzSDKService.exe
taskkill: RzSDKServer.exe

LF
say: ==================================================
say: Logitech Trim
say: ==================================================
LF
sevice: LogiFacecamService

LF
say: ==================================================
say: Realtek Trim
say: ==================================================
LF
service: DtsApo4Service
service: NahimicService # Realtek
service: RtkAudioUniversalService # Realtek

LF
say: ==================================================
say: Other Trims
say: ==================================================
LF
service: AdobeARMservice # Update service
service: Futuremark SystemInfo Service

taskkill: Agent.exe
taskkill: GameBarPresenceWriter.exe
taskkill: LibHWInfo.exe
taskkill: MicrosoftEdgeUpdate.exe
taskkill: NoiseCancelingEngine.exe
taskkill: nvfvsdksvc_x64.exe
taskkill: P508PowerAgent.exe
taskkill: ShellExperienceHost.exe
taskkill: StartMenuExperienceHost.exe
taskkill: UserOOBEBroker.exe

LF
say: ==================================================
say: Windows Trims
say: ==================================================
LF
service_template: AarSvc_ # Agent Activation Runtime
service_template: CDPUserSvc_ # Connected Devices Platform User Service
service_template: OneSyncSvc_ # This service synchronizes mail, contacts, calendar etc.
service_template: UserDataSvc_ # User Data Access
service_template: UnistoreSvc_ # Use Data Storage
service_template: WpnUserService_ # Windows Push Notifications User Service
service_template: Windows Push Notifications User Service_ # This service hosts Windows notification platform

service: AppXSvc # AppX Deployment Service
service: BthAvctpSvc # AVCTP Service
service: DiagTrack # Connected User Experiences and Telemetry
service: LanmanWorkstation # Workstation
service: RasMan # Remote Access Connection Manager
service: Spooler # Print Spooler
service: SQLWriter # SQL Server VSS Writer
service: SstpSvc # Secure Socket Tunneling Protocol Service
service: TabletInputService # Touch Keyboard and Handwriting Panel Service
service: VBoxSDS # VirtualBox
service: Windows Push Notifications System Service
service: WpnService # Windows Push Notifications System Service
service: WSearch # Windows Search

taskkill: CompatTelRunner.exe
taskkill: Microsoft.Photos.exe
taskkill: MicrosoftEdgeUpdate.exe
# taskkill: RuntimeBroker.exe
taskkill: sqlwriter.exe

LF
LF
say DONE
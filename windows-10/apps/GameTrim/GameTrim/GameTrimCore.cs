﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace GameTrim {
    public static class GameTrimCore {
        private static readonly Regex PARSE_TASK_KILLER = new Regex(@"^[Tt]askkill:\s*(.+)\s*$");
        private static readonly Regex PARSE_SAY = new Regex(@"^[Ss]ay:\s*(.+)\s*$");
        private static readonly Regex PARSE_SERVICE = new Regex(@"^[Ss]ervice:\s*(.+)\s*$");
        private static readonly Regex PARSE_SERVICE_TEMPLATE = new Regex(@"^[Ss]ervice_template:\s*(.+)\s*$");
        private static readonly CultureInfo CULTURE_INFO = new CultureInfo("en-US");

        private static readonly Regex EXCEPTION_ALREADY_EXITED = new Regex(@"Cannot process request because the process \(\d+\) has exited");

        private static string preprocessLine(string line) {
            if (line == null || line.Equals("")) return line;
            int idx = line.IndexOf("#");
            if (idx != -1) {
                return line.Substring(0, idx);
            }
            return line;
        }

        public static string[] ExtractLineParams(String line, Regex regex, int[] indeces) {
            Match matches = regex.Match(line);
            List<string> items = new List<string>();
            for (int i = 0; i < indeces.Length; i++) {
                int wantedPos = indeces[i];
                if (matches.Groups.Count < wantedPos) {
                    items.Add(matches.Groups[wantedPos].Value);
                }
                else {
                    items.Add(null);
                }
            }
            return items.ToArray();
        }
        public static string ExtractLineParam(String line, Regex regex, int index) {
            Match matches = regex.Match(line);
            if (matches.Groups.Count > index) {
                return matches.Groups[index].Value.ToString();
            }
            return null;
        }

         public static void PerformGameTrim(string pathOrig) {
            string path = pathOrig;
            if (String.IsNullOrWhiteSpace(path) || !File.Exists(path)) {
                // Try the default (app) location
                path = "SystemTrimTemplate.txt";
            }
            if(!File.Exists(path)) {
                Debugger.Print(String.Format("Couldn't find '{0}' or '{1}'. Aborting the run.", String.IsNullOrEmpty(pathOrig) ? "" : pathOrig, path));
                return;
            }

            string[] lines = File.ReadAllLines(path);
            SystemUtils.RenewProcessCache();
            SystemUtils.RenewServiceCache();
            foreach (string lineRaw in lines) {
                /**
                 * The basic processing
                 **/

                // Ignore straight out comments
                if (lineRaw.StartsWith("#")) { continue; }

                // Process the line
                string line = preprocessLine(lineRaw);
                if(String.IsNullOrWhiteSpace(line)) { continue; }

                /**
                 * Non-parametric actions
                 **/
                
                // Newline (line feed)
                if (line.Equals("LF")) { Debugger.Print(""); continue; }

                /**
                 * Parametric actions
                 **/

                // Kill a Task
                if (line.StartsWith("taskkill:", true, CULTURE_INFO)) {
                    string taskName = ExtractLineParam(line, PARSE_TASK_KILLER, 1);
                    if (String.IsNullOrWhiteSpace(taskName)) { continue; }
                    StringBuilder output = new StringBuilder();

                    try {
                        output.AppendFormat("Terminating '{0}'... ", taskName);
                        if (SystemUtils.TaskKill(taskName)) {
                            output.Append("OK!");
                        }
                        else {
                            output.Append("not found");
                        }
                    }
                    catch (Win32Exception e) {
                        output.AppendFormat("Can't do.\n  W32Exception: {0}", e.ToString());
                    }
                    catch (NotSupportedException e) {
                        output.AppendFormat("Can't do.\n  NotSupportedException: {0}", e.ToString());
                    }
                    catch (InvalidOperationException e) {
                        string exceptionString = e.ToString();

                        // Skip some exceptions
                        if (!EXCEPTION_ALREADY_EXITED.IsMatch(exceptionString)) {
                            output.AppendFormat("Can't do.\n  InvalidOperationException: {0}", exceptionString);
                        }
                        else {
                            output.Append("OK!");
                        }
                    }

                    Debugger.Print(output.ToString());
                    continue;
                }

                // Output
                if (line.StartsWith("say:", true, CULTURE_INFO)) {
                    string say = ExtractLineParam(line, PARSE_SAY, 1);
                    if (!String.IsNullOrWhiteSpace(say)) {
                        Debugger.Print(say);
                    }
                    continue;
                }

                if (line.StartsWith("service:", true, CULTURE_INFO)) {
                    string service = ExtractLineParam(line, PARSE_SERVICE, 1);
                    service = service.Trim();
                    if (!String.IsNullOrWhiteSpace(service)) {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("Stopping service {0}...", service);
                        SystemUtils.StopService(service, null, sb);
                        Debugger.Print(sb.ToString());
                    }
                    continue;
                }

                if (line.StartsWith("service_template:", true, CULTURE_INFO)) {
                    string serviceTemplate = ExtractLineParam(line, PARSE_SERVICE_TEMPLATE, 1);
                    serviceTemplate = serviceTemplate.Trim();
                    if (!String.IsNullOrWhiteSpace(serviceTemplate)) {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("Stopping service by template {0}...", serviceTemplate);
                        SystemUtils.StopServiceByTemplate(serviceTemplate, sb);
                        Debugger.Print(sb.ToString());
                    }
                    continue;
                }
            }
        }
    }
}

﻿
namespace GameTrim {
    partial class GameTrim {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.theGameTrimTrigger = new System.Windows.Forms.Button();
            this.outputTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // theGameTrimTrigger
            // 
            this.theGameTrimTrigger.Location = new System.Drawing.Point(13, 13);
            this.theGameTrimTrigger.Name = "theGameTrimTrigger";
            this.theGameTrimTrigger.Size = new System.Drawing.Size(775, 23);
            this.theGameTrimTrigger.TabIndex = 0;
            this.theGameTrimTrigger.Text = "Perform the Game Trim";
            this.theGameTrimTrigger.UseVisualStyleBackColor = true;
            this.theGameTrimTrigger.Click += new System.EventHandler(this.theGameTrimTrigger_Click);
            // 
            // outputTextBox
            // 
            this.outputTextBox.Location = new System.Drawing.Point(13, 43);
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.Size = new System.Drawing.Size(775, 395);
            this.outputTextBox.TabIndex = 1;
            this.outputTextBox.Text = "";
            // 
            // GameTrim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.outputTextBox);
            this.Controls.Add(this.theGameTrimTrigger);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GameTrim";
            this.Text = "Game Trim";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button theGameTrimTrigger;
        private System.Windows.Forms.RichTextBox outputTextBox;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameTrim {
    class SystemUtils {
        private static Dictionary<string, Process> cachedProcesses = null;
        private static Dictionary<string, ServiceController> cachedServices = null;

        public static void RenewServiceCache() {
            if (cachedServices != null) {
                cachedServices.Clear();
                cachedServices = null;
            }

            cachedServices = new Dictionary<string, ServiceController>();
            ServiceController[] services = ServiceController.GetServices();

            foreach(ServiceController service in services) {
                if(service == null || String.IsNullOrEmpty(service.ServiceName)) { continue; }
                cachedServices[service.ServiceName] = service;
            }
        }

        public static bool StopService(string name, ServiceController service, StringBuilder report) {
            try {
                if (service == null) {
                    service = new ServiceController(name);
                }

                if (service == null) {
                    report.Append("couldn't instantiate ServiceController().");
                    return false;
                }

                if (service.Status.Equals(ServiceControllerStatus.Running)) {
                    if (service.CanStop) {
                        TimeSpan timeout = TimeSpan.FromMilliseconds(3000);

                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                        if (report != null) {
                            report.Append("OK");
                        }
                        return true;
                    }
                    else {
                        if (report != null) {
                            report.Append("Can't stop.");
                        }
                        return false;
                    }
                }
                else if (service.Status.Equals(ServiceControllerStatus.Stopped)) {
                    if (report != null) {
                        report.Append("already stopped.");
                    }
                }
                else {
                    if (report != null) {
                        report.AppendFormat("can't handle the status '{0}'", service.Status.ToString());
                    }
                }

                return true;
            }
            catch(Exception e) {
                if (report != null) {
                    string eString = e.ToString();
                    if(eString.Contains("was not found on computer")) {
                        report.Append("not present.");
                    }
                    else {
                        report.AppendFormat("Error while stopping service '{0}': {1}", name, e.ToString());
                    }
                }
            }
            return false;
        }

        public static bool StopServiceByTemplate(string tpl, StringBuilder report) {
            Regex tplRegex = new Regex(String.Format("{0}[a-zA-Z0-9]+", tpl));

            if(cachedServices == null) { RenewServiceCache(); }
            foreach(KeyValuePair<string, ServiceController> item in cachedServices) {
                string candidateService = item.Key;
                if(tplRegex.IsMatch(candidateService)) {
                    report.AppendFormat("\n  Found '{0}'. Stopping... ", candidateService);
                    ServiceController sc = item.Value;
                    return StopService(candidateService, sc, report);
                }
            }

            report.Append("not found.");
            return false;
        }

        public static void RenewProcessCache() {
            if (cachedProcesses != null) {
                cachedProcesses.Clear();
                cachedProcesses = null;
            }

            cachedProcesses = new Dictionary<string, Process>();
            Process[] rawProcesses = Process.GetProcesses();
            foreach (Process process in rawProcesses) {
                try {
                    string locatedName = process.MainModule.ModuleName;
                    cachedProcesses[locatedName] = process;
                    continue;
                }
                catch (Exception e) {
                    Debug.Print(String.Format("Error while caching the list of present processes: {0}", e.ToString()));
                }
                string locatedName2 = process.ProcessName + ".exe";
                cachedProcesses[locatedName2] = process;
            }
        }

        private static Process FindProcess(String name) {
            if(cachedProcesses == null) { RenewProcessCache(); }
            return cachedProcesses.ContainsKey(name) ? cachedProcesses[name] : null;
        }

        public static bool TaskKill(string name) {
            Process process = FindProcess(name);
            if (process == null) {
                return false;
            }
            process.Kill();
            return true;
        }

        public static void DisableMemoryCompresion() {
            // TODO
        }
    }
}
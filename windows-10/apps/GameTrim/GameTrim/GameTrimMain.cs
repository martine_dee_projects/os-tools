﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace GameTrim {
    public partial class GameTrim : Form {
        private string inputFile = null;

        public GameTrim() : this(null) {}

        public GameTrim(string inputFile) {
            this.inputFile = inputFile;
            InitializeComponent();
            CustomInit();
        }

        private void CustomInit() {
            this.outputTextBox.ReadOnly = true;
            Debugger.rtDebug = this.outputTextBox;
            Debugger.debugLevelThreshold = 0;
        }

        private void theGameTrimTrigger_Click(object sender, EventArgs e) {
            this.theGameTrimTrigger.Enabled = false;
            string path = inputFile == null
                ? @"SystemTrimTemplate.txt"
                : inputFile
            ;
            GameTrimCore.PerformGameTrim(path);
            this.theGameTrimTrigger.Enabled = true;
        }
    }
}

﻿using System;
using System.Windows.Forms;

namespace GameTrim {

    static class Program {
        /// <summary>
        /// The main entry point for the Game Trim. This program shuts down
        /// Windows 10 tasks and services, based off a given list. This should
        /// provide for a better gaming experience. The program has rudimentary
        /// output, via say $string, and LF (for a newline). Check out the
        /// sample template in ./Templates/ .
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Debugger.logPath = "win10gt.log";

            string file = null;
            bool noUI = false;

            foreach (string arg in args) {
                if (arg.StartsWith("--file=")) {
                    file = arg.Substring(7);
                    continue;
                }
                if (arg.Equals("--noui")) {
                    noUI = true;
                }
            }

            if (noUI) {
                // E.g.
                // --file="C:\Users\{YOU}\SystemTrimTemplate.txt" --noui
                GameTrimCore.PerformGameTrim(file);
            }
            else {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new GameTrim(file));
            }
        }
    }
}

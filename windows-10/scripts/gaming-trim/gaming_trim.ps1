# ============================================
# The large service trim, in a separate script
# ============================================
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path

Write-Output "Running one pass of the trim_services.pl"
perl "$ScriptDir\trim_services.pl"

# ============================================
# Disables memory compression
# ============================================
Write-Output "Disabling memory compression"
Disable-MMAgent -mc

# ============================================
# All unwanted tasks go here
# ============================================
Write-Output "Killing tasks"

taskkill /IM "Agent.exe" /F
taskkill /IM "ArmourySocketServer.exe" /F
taskkill /IM "ArmourySwAgent.exe" /F
taskkill /IM "asus_framework.exe" /F
taskkill /IM "AsusUpdate.exe" /F
taskkill /IM "CueLLAccessService.exe" /F
taskkill /IM "LibHWInfo.exe" /F
taskkill /IM "MicrosoftEdgeUpdate.exe" /F
taskkill /IM "NoiseCancelingEngine.exe" /F
taskkill /IM "nvfvsdksvc_x64.exe" /F
taskkill /IM "P508PowerAgent.exe" /F
taskkill /IM "ROGLiveService.exe" /F
taskkill /IM "StartMenuExperienceHost.exe" /F
taskkill /IM "ShellExperienceHost.exe" /F
taskkill /IM "MicrosoftEdgeUpdate.exe" /F

# Razer
taskkill /IM "FPSRunner32.exe" /F
taskkill /IM "FPSRunner64.exe" /F
taskkill /IM "GameManagerService.exe" /F
taskkill /IM "RzKLService.exe" /F
taskkill /IM "RazerCortex.exe" /F
taskkill /IM "Razer Synapse Service.exe" /F
taskkill /IM "Razer Synapse 3.exe" /F
taskkill /IM "RzSDKService.exe" /F
taskkill /IM "RzSDKServer.exe" /F
taskkill /IM "RazerCentralService.exe" /F
taskkill /IM "Razer Central.exe" /F
taskkill /IM "PMRunner64.exe" /F
taskkill /IM "PMRunner32.exe" /F
taskkill /IM "Razer Synapse Service Process.exe" /F

Write-Output "Press enter to continue."
pause

# ============================================
# Loops through comeback services
# ============================================
Start-Sleep -Seconds 10.0

Write-Output "Looping over comeback services"
perl "$ScriptDir\trim_services.pl" --stop_comebacks
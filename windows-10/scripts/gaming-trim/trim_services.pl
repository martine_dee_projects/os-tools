# This Perl script shuts down Windows 10 services and ex-
# ecutables that are not used in many gaming scenarios.
# Trimming them off should in theory improve the gaming
# experience. I use this personally for Quake Champions
# only, as it is a game that easily hits CPU bottleneck
# even with the top-shelft CPUs one can get at the moment.
#
# The shutdowns are temprary, until the next restart.
#
# NOTE: Generally this script is intended to be invoked
# via gaming_trim.pl. In other words, you wouldn't normally
# invoke it directly.
#
# NOTE: In order to run this cript, you need to have Perl
# installted on your system (5.x).
#
# To modify what gets shut down, have a look at the sub
# get_all_services() .

use strict;
use warnings;

# A simple check whether we are running this on (any)
# Windows. However, the error message clarifies even fur-
# ther, that this script is only made to work with
# Windows 10.

unless((($^O) || '') eq 'MSWin32') {
    print STDERR "This script is only valid on Windows 10!

Are you running Windows 10?

Then you can remove the code-block where this message is,
to enable running the script past this check.

";
    exit 1;
}

# Script switches
my $DEBUG_OUTPUT = 0;
my $STOP_COMEBACKS = 0;

# Process the input params
$STOP_COMEBACKS = 1 if(int(@ARGV) == 1 && $ARGV[0] eq '--stop_comebacks');

#
# Statistic variables on what the script is about to accomplish
#

# Stats & Logging
my $count_stopped = 0;
my $count_already_down = 0;
my $count_failed_to_stop = 0;

# The list of services that failed to stop
my @services_failed_to_stop;
my @stopped_services;

#
# Script's Workflow
#

# Switch for looping minutely to shut down comebacks.
# Note: By 'comeback' is meant: a service that keeps
# coming back up.
if($STOP_COMEBACKS) {
    my $the_minute = 1;
    my $services = query_services(labels => [qw(comeback)]);
    while(1) {
        print "====================================================
The minute $the_minute\n\n";
        stop_services($services);
        print_the_stats();
        clear_the_stats();
        sleep 60;
        $the_minute++;
    }
}
else {
    stop_services(query_services());
    print_the_stats();
}

exit;

# =================================================================================================
# === The script flow ends below this line. Below are the helpers and the data. ===================
# =================================================================================================

#
# This sub stops all services it is given
#

sub stop_services {
    my ($services) = @_;
    for my $a_service (@{$services // []}) {
        stop_a_service($a_service);
    }
}

# The main subroutine that stops a single service, and logs
# information relevant to what is going on. If you were to
# use it atomically, you could check return value from the
# subroutine.

sub stop_a_service {
    my ($name, %params) = @_;

    my $silent = $params{silent}; # TODO: Propagate to invoked commands (non-trivial)

    print "==========================================
Stopping: '$name'\n" if $DEBUG_OUTPUT && !$silent;
    my $raw_output = '';
    if($name eq 'gpsvc') {
        $raw_output = `net stop gpsvc`;
    }
    else {
        $raw_output = `sc stop "$name"`;
    }

    # Process the result of the action

    my $stopped_successfully = 0;
    my $was_down = 0;
    my $failed_to_stop = 0;

    # Stats and status check
    if($raw_output =~ /The service has not been started./) {
        $count_already_down++;
        $was_down = 1;
    }
    elsif(
        $raw_output =~ /^Stopped/
     || $raw_output =~ /STOP_PENDING/
     || $raw_output =~ /STOPPED/
    ) {
        $count_stopped++;
        $stopped_successfully = 1;
        push @stopped_services, $name;
    }
    else {
        $count_failed_to_stop++;
        $failed_to_stop = 1;
        push @services_failed_to_stop, $name;
    }

    # Messaging
    if($stopped_successfully) {
        print "Stopped $name\n" unless $silent;
    }
    elsif(!$was_down) {
        print "Couldn't stop $name\n" unless $silent;
        print "OUTPUT:\n\n" . $raw_output unless $silent;
    }

    if($DEBUG_OUTPUT && ($was_down || $stopped_successfully)) {
        print "OUTPUT:\n\n" . $raw_output unless $silent;
    }

    # Tell what happened, in case that the caller wanted to know
    return {
        service_name => $name,
        stopped_successfully => $stopped_successfully,
        was_down => $was_down,
        failed_to_stop => $failed_to_stop,
        raw_output => $raw_output,
    };
}

#
# Print the stats
#

sub print_the_stats {
    # Only print stats on failed stops if there were such
    if($count_failed_to_stop) {
        print "Failed:       $count_failed_to_stop\n";
        for my $failed_service (sort { $a cmp $b } @services_failed_to_stop) {
            print " - $failed_service\n";
        }
        print "\n";
    }
    print "Stopped:      $count_stopped\n";
    if(int(@stopped_services)) {
        for my $stopped_service (sort { $a cmp $b } @stopped_services) {
            print " - $stopped_service\n";
        }
        print "\n";
	}
    print "Already down: $count_already_down\n";
}

#
# Clear the stats (get ready for another round of stopping services)
#

sub clear_the_stats {
    $count_stopped = 0;
    $count_already_down = 0;
    $count_failed_to_stop = 0;
    @services_failed_to_stop = ();
    @stopped_services = ();
}

# A simple querier for the service lists.

sub query_services {
    my %params = @_;

    # Process input parameters
    my $include_templates = !$params{no_templates};
    my $labels = $params{labels};

    # Obtain the list of all services known to the system
    my @all_services = @{get_all_serivces()};
    my %all_services = map { $_ => 1 } @all_services;

    # Obtain the list of all watched services
    my @watched_services = @{get_all_watched_services()};

    # Split them into full-name vs. templated services
    my @full_name_watched_services;
    my @templated_watched_services;
    for my $watched_service (@watched_services) {
        if((ref($watched_service) eq 'HASH') && $watched_service->{_template}) {
            push @templated_watched_services, $watched_service if $include_templates;
        }
        else {
            push @full_name_watched_services, $watched_service;
        }
    }

    # BEWARE, some services are stored as hashes. Service name
    # for such a service is under the {name} key.

    my @services_to_trim;
    # Cull the rest if there is nothing to work with
    return \@services_to_trim unless int(@full_name_watched_services) || int(@templated_watched_services);

    # Common sub for prepping a service for the service name list
    my $prep_a_service = sub {
        my ($watched_service) = @_;
        if($watched_service && (ref($watched_service) eq 'HASH')) {
            my $name = $watched_service->{name};
            my $all_labels_passed = 1;
            if($labels) {
                for my $label (@$labels) {
                    if(!$watched_service->{$label}) {
                        $all_labels_passed = 0;
                        last;
                    }
                }
            }
            if($all_labels_passed) {
                push @services_to_trim, $name;
            }
        }
        elsif(!$labels) {
            push @services_to_trim, $watched_service;
        }
    };

    # Prep the full-name services
    {
        for my $full_name_watched_service (@full_name_watched_services) {
            next unless $full_name_watched_service;
            my $name = (ref($full_name_watched_service) eq 'HASH')
                ? $full_name_watched_service->{name}
                : $full_name_watched_service
            ;
            next unless $name && $all_services{$name};
            $prep_a_service->($full_name_watched_service);
        }
    }

    # Prep the templated services
    {
        # We need actual names of the templated services
        my %templated_watched_service_tpl_names =
            map { $_->{tpl_name} => $_ }
            grep { ref($_) eq 'HASH' }
            @templated_watched_services
        ;

        # Now we need to know which of those are known to the system
        for my $running_service_name (@all_services) {
            if($running_service_name && $running_service_name =~ /(.+)_[0-9a-fA-F]{5}[0-9a-fA-F]?$/) {
                my $tpl_name = $1 . '_';
                if(my $templated_watched_service = $templated_watched_service_tpl_names{$tpl_name}) {
                    $templated_watched_service->{name} = $running_service_name;
                    $prep_a_service->($templated_watched_service);
                }
            }
        }
    }

    # Return the result
    return \@services_to_trim;
}

# Gets the list of all services known to sc (windows command)

sub get_all_serivces { # TODO: Turn into get_running_services (also by functionality)
  my $all_services_raw = `sc queryex type=service state=all`;
  my @lines_raw = split /\n/, $all_services_raw;

  my @services;
  for my $line (@lines_raw) {
    if($line =~ /SERVICE_NAME: (.+)/) {
      push @services, $1;
    }
  }

  return \@services;
}

#
# The list of services that are otherwise needed, but can
# be stopped during gaming. This list only includes the
# services that can be named by the full name. E.g., there
# are also services whose name is appended with a 6-7 hex
# digit number, which should be listed in
#
#   get_user_services_to_trim()
#
# instead.
#

sub get_all_watched_services { [

# ==========================================================
# Templated service names
# ==========================================================
#
# This section contains templates for the names of ser-
# vices whose name is appended with a 5-6 hex digit number.
#
# These services have to be stopped by a name that is de-
# termined per windows session. Put them all here.
#
# All of those services might be otherwise needed, but not
# for gaming.
{ tpl_name => 'AarSvc_', _template => 1 }, # Agent Activation Runtime
{ tpl_name => 'OneSyncSvc_', _template => 1 }, # Sync Host
{ tpl_name => 'UserDataSvc_', _template => 1 }, # User Data Access
{ tpl_name => 'UnistoreSvc_', _template => 1 }, # Use Data Storage
{ tpl_name => 'WpnUserService_', _template => 1, comeback => 1 }, # Windows Push Notifications User Service

# Windows unnecessary
'RasMan', # Remote Access Connection Manager
'SstpSvc', # Secure Socket Tunneling Protocol Service
{name => 'WSearch', comeback => 1}, # Windows Search
'Spooler', # Print Spooler
'LanmanWorkstation', # Workstation

# Windows
'AppXSvc', # AppX Deployment Service
'DiagTrack', # Connected User Experiences and Telemetry
'TabletInputService', # Touch Keyboard and Handwriting Panel Service
'WpnService', # Windows Push Notifications System Service

# Razer
'RzKLService',
'Razer Synapse Service',
'Razer Game Manager Service',
'Razer Chroma SDK Service',
'Razer Central Service',

# Adobe
'AdobeARMservice', # Update service

# Asus
# 'asComSvc', # Asus COM Service
# 'asHmComSvc', # Asus HM Com Service
'ROG Live Service', # ROG Live Service

# Realtek
'NahimicService', # Realtek
'RtkAudioUniversalService', # Realtek

# Corsair (iCUE)
'CorsairGamingAudioConfig', # Corsair Gaming Audio Configuration Service
{name => 'CorsairLLAService', comeback => 1}, # Corsair iCUE Helper Service

# Realtek
'DtsApo4Service',

# Apps
'Futuremark SystemInfo Service',
'LightingService', # NVIDIA, Asus or Corsair?

]; }

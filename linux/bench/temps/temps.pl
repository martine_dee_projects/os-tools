use strict;
use warnings;

use Data::Dumper qw(Dumper);
use Time::Stamp qw(gmstamp);

# Writing to a file: careful, it might fail for invalid paths
my $OUTPUT_FILE = $ENV{OUTPUT_FILE};

# All output gets accumulated in the $output
my $output = '';

# The timestamp
{
    $output .= sprintf("--\n%s\n", gmstamp());
}

# Collect CPU stats
{
    my $core_temps = collect_cpu_temps();
    my @core_names = sort {$a <=> $b} keys %$core_temps;
    if(int(@core_names)) {
        my @temps = map { $core_temps->{$_} } @core_names;
        $output .= "CPU(s)\n  " . join('; ', @temps) . "\n";
    }
}

# Collect GPU stats
{
    my $graphic_info = collect_nvidia_temps();
    if(int($graphic_info)) {
        $output .= "GPU(s)\n";
        for my $info (@$graphic_info) {
            $output .= sprintf(
                "%s; %s; %s\n",
                $info->{driver_version} // '',
                $info->{product_name} // '',
                $info->{gpu_mark} // '',
            );
            $output .= sprintf(
                "  %s; %s\n",
                $info->{current_temp} // '',
                $info->{power_draw} // '',
            );
        }
    }
}

# Print
if($OUTPUT_FILE) {
    open my $fout, '>>', $OUTPUT_FILE or die qq{Couldn't write to '$OUTPUT_FILE': } . $!;
    print {$fout} $output;
    close $fout or die qq{Couldn't close '$OUTPUT_FILE': } . $!;
}
else {
    print $output;
}

exit; # The end of the work

sub collect_cpu_temps {
    my %core;
    my $output_sensors = `sensors`;
    my @lines = split("\n", $output_sensors);
    for my $line (@lines) {
        if($line =~ /Core (\d+)\:\s+\+?(\d+\.\d+)/) {
            my $id = $1;
            my $temp = $2;
            $core{$id} = $temp;
        }
    }
    return \%core;
}

sub collect_nvidia_temps {
    my $nvidia_output = `nvidia-smi -q -a`;
    my @lines = split("\n", $nvidia_output);

    my $gpu_mark;
    my $product_name;
    my $power_draw;
    my $current_temp;
    my $driver_version;

    my @output;

    my $append_output = sub {
        return unless $gpu_mark;
        push @output, {
            product_name => $product_name,
            gpu_mark => $gpu_mark,
            current_temp => $current_temp,
            power_draw => $power_draw,
            driver_version => $driver_version,
        };
    };
    for my $line (@lines) {
        if($line =~ /Driver Version\s+:\s?([^\s]+)$/) {
            $append_output->();
            $driver_version = $1;
            undef($product_name);
            undef($power_draw);
            undef($current_temp);
            undef($gpu_mark);
        }
        elsif($line =~ /Product Name\s+:\s?(.+)$/) {
            $product_name = $1;
        }
        elsif($line =~ /Power Draw\s+:\s?(\d+\.\d+ W)$/) {
            $power_draw = $1;
        }
        elsif($line =~ /GPU Current Temp\s+:\s?(\d+ C)$/) {
            $current_temp = $1;
        }
        elsif($line =~ /^GPU (.+)$/) {
            $gpu_mark = $1;
        }
    }
    $append_output->();

    return \@output;
}
